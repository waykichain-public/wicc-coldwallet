DATA_DIR=~/MyColdWallet/data
mkdir -p $DATA_DIR
cd ~/MyColdWallet/wicc-coldwallet/docker
cp ../coolwalletprod.db $DATA_DIR/

docker stop wicc-mcw-nodejs && docker rm wicc-mcw-nodejs

docker run -d --restart always \
    --name "wicc-mcw-nodejs" \
    -v $DATA_DIR:/sqlite \
    -p 5000:5000 \
    wicc/mcw-nodejs;
