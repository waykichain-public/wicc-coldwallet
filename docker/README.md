# Introduction
In order to simplify the installation of a cold wallet, two docker contains are established such that one is to provide the front-end web UI and the other to provide the backend
service with wallet database and management features.

# Installation Procedures

## 1. create a dedicated wallet home directory at one's home director

```mkdir -p ~/MyColdWallet/data```

## 2. clone this project into the wallet home dir

```cd ~/MyColdWallet && git clone https://gitlab.com/waykichain-public/wicc-coldwallet.git```

## 3. Update two source files to use ```mainnet``` for production

 * WebUI: ```./webui/src/tools/config.js```
 * API Server: ```./server/utils/config.js```

## 4. build the backend container image

```cd ~/MyColdWallet/wicc-coldwallet/docker && sh ./build-nodejs-docker.sh```

## 5. copy mainnet d/b into the data dir

```cp ~/MyColdWallet/wicc-coldwallet/coolwalletprod.db ~/MyColdWallet/data/```

## 6. launch wallet backend service docker

```cd ~/MyColdWallet/wicc-coldwallet/docker && sh ./run-mcw-nodejs-docker.sh```

## 7. launch wallet frontend webui docker

```cd ~/MyColdWallet/wicc-coldwallet/docker && sh ./run-mcw-nginx-docker.sh```

## 8. Last step - verify installation - launch cold wallet webapp

open Safari browser on your MAC, type in ```http://localhost:8080``` to open it!