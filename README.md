# All-in-one Dockerized Installation
```
mkdir -p ~/MyColdWallet/data
cd ~/MyColdWallet && git clone https://gitlab.com/waykichain-public/wicc-coldwallet.gi
cd ~/MyColdWallet/wicc-coldwallet/docker
sh build-node-js-docker.sh
sh run-mcw-nodejs-docker.sh
run-mcw-nginx-docker.sh
```

# Switch Mainnet | Testnet

## Two files must be updated accordingly

 * WebUI: ```./webui/src/tools/config.js```
 * API Server: ```./server/utils/config.js```