const express = require('express')
const cors = require("cors")

const router = require('./router')
const bodyParser = require('body-parser')
const app = express()

var log4js = require('log4js');
log4js.configure('./config/log4js.json');

app.use(cors())
    // 放在路由前
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

//设置跨域请求
app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:8080");
    res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
    res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
    res.header("Access-Control-Allow-Credentials", true);
    res.header("X-Powered-By", ' 3.2.1')
    if (req.method == "OPTIONS") res.send(200); /*让options请求快速返回*/
    else next();
});


app.use('/', router)

var http = require("http");

//创建一个服务器，回调函数表示接收到请求之后做的事情
var server = http.createServer(app);
//监听端口
server.listen(5000, "0.0.0.0", function() {
    const { address, port } = server.address()
    console.log('HTTP服务启动成功：', address, port)
});