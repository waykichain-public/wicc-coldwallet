let NETTYPE = 'testnet' //正式环境mainnet  //测试环境 testnet
let baseUrl = 'http://127.0.0.1:5000' //api 接口
let rawTxUrl = 'https://baas-test.wiccdev.org/v2/api/submitrawtx/' //签名路径: https://baas-test.wiccdev.org/v2/api/submitrawtx/  正式环境：https://baas.wiccdev.org/v2/api/submitrawtx/

export {
    NETTYPE,
    baseUrl,
    rawTxUrl
}